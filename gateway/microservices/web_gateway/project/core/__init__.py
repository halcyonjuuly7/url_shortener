import aiohttp
from sanic import Sanic
from typing import Optional

def app_factory(config_path:Optional[str]=None):
    app = Sanic(__name__)
    if config_path is not None:
        app.config.from_pyfile(config_path)

    from .blueprints import account
    from .blueprints import authorization
    from .blueprints import profile
    from .blueprints import authentication
    from .blueprints import key_service

    app.blueprint(authentication, url_prefix="/authentication")
    app.blueprint(authorization, url_prefix="/authorization")
    app.blueprint(key_service, url_prefix="/key_service")
    app.blueprint(account, url_prefix="/accounts")
    app.blueprint(profile, url_prefix="/profile")

    @app.listener("before_server_start")
    async def setup(app, loop):
        app.aiohttp_session = aiohttp.ClientSession()

    @app.listener("after_server_stop")
    async def teardown(app, loop):
        app.aio_session.close()

    return app