import functools
import aiohttp
from sanic import response
from sanic_oauth.providers import GoogleClient, FacebookClient
from typing import Dict


def token_required(f):
    # @functools.wraps(f)
    async def wrapper(*args, **kwargs):
        request = args[0]
        async with request.app.aiohttp_session.post(f"{app.config['SERVICES_ROOT']['authentication']}/decode_token",
                                            json={"token": request.headers.get("auth_token")}) as auth_resp:
            data = await auth_resp.json()
            if data['status'] == "success":
                request.payload = data["payload"]
                func_resp = await f(*args, **kwargs)
            else:
                func_resp = response.redirect(f"{app.config['SERVICES_ROOT']}/login")
            return func_resp
    return wrapper


def no_login(f):
    # @functools.wraps(f)
    async def wrapper(*args, **kwargs):
        request = args[0]
        async with request.app.aiohttp_session.post(f"{app.config['SERVICES_ROOT']['authentication']}/validate_token",
                                                    json={"token": request.json.get("token")}) as auth_resp:
            data = await auth_resp.json()
            if data['status'] == "success":
                func_resp = response.redirect(f"{app.config['SERVICES_ROOT']['account']}/login")
            else:
                func_resp = await f(*args, **kwargs)
            return func_resp
        return wrapper

def oauth_client_factory(provider:str, session:aiohttp.ClientSession, **kwargs:Dict[str, str]):
    client = None
    if provider == "facebook":
        client = FacebookClient(session, **kwargs)
    elif provider == "google":
        client = GoogleClient(session, **kwargs)
    return client




