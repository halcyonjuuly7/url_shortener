from . import authentication
from sanic import response

@authentication.route("/validate_token", methods=("POST",))
async def validate_token(request):
    root_url = f'{request.app.config["SERVICES_ROOT"]["user_service"]}/authentication/validate_token'
    async with request.app.aiohttp_session.post(root_url,
                                       json={"auth_token": request.json.get("auth_token")}) as resp:
        data = await resp.json()
        return response.json(data)


@authentication.route("/blacklist_token", methods=("POST",))
async def blacklist_token(request):
    url = f"{request.app.config['SERVICES_ROOT']['user_service']}/authentication/blacklist_token"
    async with request.app.aiohttp_session.post(url, json=request.json) as resp:
        data = await resp.json()
        return response.json(data)

@authentication.route("/create_token", methods=("POST", ))
async def create_token(request):
    url = f"{request.app.config['SERVICES_ROOT']['user_service']}/authentication/create_token"
    async with request.app.aiohttp_session.post(url, json=request.json) as resp:
        data = await resp.json()
        return response.json(data)

@authentication.route("/decode_token", methods=("POST",))
async def decode_token(request):
    url = f"{request.app.config['SERVICES_ROOT']['user_service']}/authentication/decode_token"
    async with request.app.aiohttp_session.post(url, json=request.json) as resp:
        data = await resp.json()
        return response.json(data)



