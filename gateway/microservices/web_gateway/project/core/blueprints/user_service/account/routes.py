import datetime
from . import account
from sanic import response

@account.route("/create", methods=("POST",))
async def create_account(request):
    create_acc = f"{request.app.config['SERVICES_ROOT']['user_service']}/accounts/create"
    async with request.app.aiohttp_session.post(create_acc, json=request.json) as acc_resp:
        data = await acc_resp.json()
        if data['status'] == 'success':
            create_token = f"{request.app.config['SERVICES_ROOT']['user_service']}/authentication/create_token"
            async with request.app.aiohttp_session.post(create_token,
                                                        json={'email': request.json.get('email'),
                                                              'first_name': request.json.get('first_name'),
                                                              'last_name': request.json.get('last_name'),
                                                              'profile_id': data['profile_id']}) as token_resp:
                token_data = await token_resp.json()
                return response.json(token_data)
        return response.json(data)


@account.route("/info", methods=("POST"))
async def account_info(request):
    acc_info = f"{request.app.config['SERVICES_ROOT']['user_service']}/accounts/info"
    async with request.app.aiohttp_session.get(acc_info, params=request.args) as resp:
        data = await resp.json()
        return response.json(data)


@account.route("/check_user_password", methods=("POST", ))
async def check_user_password(request):
    acc_info = f"{request.app.config['SERVICES_ROOT']['user_service']}/accounts/check_user_password"
    async with request.app.aiohttp_session.post(acc_info, json=request.json) as resp:
        data = await resp.json()
        if data["status"] == "success":
            acc_info = f"{request.app.config['SERVICES_ROOT']['user_service']}/profile/info"
        return response.json(data)


@account.route("/login_user", methods=("POST",))
async def account_login(request):
    check_pass_url  = "http://user_service:5555/accounts/check_user_password"
    async with request.app.aiohttp_session.post(check_pass_url,
                                                json={"email": request.json.get("email"),
                                                      "password": request.json.get("password")}) as check_pass:
        check_pass_data = await check_pass.json()
    if check_pass_data["status"] == "success":
        prof_id_url = "http://user_service:5555/accounts/user_info"
        async with request.app.aiohttp_session.get(prof_id_url,
                                                   params={"email": request.json.get("email")}) as prof_id:
            prof_id_data = await prof_id.json()

        if prof_id_data["status"] == "success":
            create_token_url = "http://user_service:5555/authentication/create_token"
            data = prof_id_data.pop("data")
            data["iat"] = datetime.datetime.utcnow().timestamp()
            async with request.app.aiohttp_session.post(create_token_url,
                                                    json=data) as create_token:
                create_token_data = await create_token.json()
                return response.json(create_token_data)
    return response.json(check_pass_data)