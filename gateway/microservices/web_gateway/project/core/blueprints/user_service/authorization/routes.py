from . import authorization
from sanic import response
from core.helpers.auth import no_login

@authorization.route("/oauth2/authorize/<provider>", methods=("POST",))
# @no_login
async def oauth_authorize(request, provider):
    url = f"{request.app.config['SERVICES_ROOT']['user_service']}/authorization/oauth2/authorize/{provider}"
    async with request.app.aiohttp_session.post(url,json=request.json) as auth_resp:
        data = await auth_resp.json()
        return response.json(data)



@authorization.route("/oauth2/callback/<provider>", methods=("POST",))
# @no_login
async def oauth2_callback(request, provider):
    oaut2_cb = f"{request.app.config['SERVICES_ROOT']['user_service']}/authorization/oauth2/callback/{provider}"
    async with request.app.aiohttp_session.post(oaut2_cb,json=request.json) as resp:
        oaut2_cb_data = await resp.json()

    if oaut2_cb_data["status"] == "success":
        create_token = f"{request.app.config['SERVICES_ROOT']['user_service']}/authentication/create_token"
        async with request.app.aiohttp_session.post(create_token,
                                                    json=oaut2_cb_data["user_info"]) as create_token_resp:
            token_data = await create_token_resp.json()
        return response.json(token_data)
    return response.json({"status": "error"})