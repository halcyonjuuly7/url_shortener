from . import profile
from core.helpers.auth import token_required
from sanic import response

@profile.route("/info")
async def profile_index(request):
    async with request.app.aiohttp_session.get(f"http://key_service:5555/profile/{request.args.get('profile_id')}") as resp:
        json_data = await resp.json()
    async with request.app.aiohttp_session.get(f"{request.app.config['SERVICES_ROOT']['user_service']}/profile/info",
                                               params={"profile_id": request.args.get("profile_id")}) as profile_data:
        data = await profile_data.json()
        data["urls"] = json_data["urls"]
        return response.json(data)


