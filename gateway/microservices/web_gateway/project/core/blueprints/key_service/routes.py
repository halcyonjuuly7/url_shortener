from . import key_service
from sanic import response
from core.helpers.auth import token_required

@key_service.route("/anon_generate_key", methods=("POST",))
async def generate_hash(request):
    payload = {**request.json}
    payload['owner'] = -1
    async with request.app.aiohttp_session.post("http://key_service:5555/generate_key",
                                                json=payload) as resp:
        data = await resp.json()
        return response.json(data)


@key_service.route("/generate_key", methods=("POST",))
@token_required
async def generate_hash(request):
    payload = {**request.json}
    async with request.app.aiohttp_session.get("http://user_service:5555/profile_info",
                                               params={"email": request.payload["sub"]}) as resp:
        prof_data = await resp.json()
        payload["owner"] = prof_data["id"]
    async with request.app.aiohttp_session.post("http://key_service:5555/generate_key",
                                                json=payload) as resp:
        data = await resp.json()
        return response.json(data)