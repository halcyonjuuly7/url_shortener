SERVICES_ROOT = {
    "authentication": "http://authentication:5555",
    "authorization": "http://authorization:5555",
    "user_service": "http://user_service:5555",
    "key_service": "http://key_service:5555"
}

OAUTH2_CREDS = {
    "facebook": {
        "client_id": "",
        "client_secret": ""
    },
    "google": {
        "client_id": "",
        "client_secret": ""
    }
}