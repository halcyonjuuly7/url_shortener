import aioredis
import asyncio
import aiopg
import aiohttp

from typing import Optional
from sanic import Sanic



async def prep_db(dsn):
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as curr:
                # await curr.execute("""
                # SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower('accounts');
                # """)
                # resp = [row async for row in curr]
                # if not resp:
                await curr.execute("DROP TABLE IF EXISTS ACCOUNTS CASCADE;")
                await curr.execute("DROP TABLE IF EXISTS PROFILES CASCADE;")
                await curr.execute("DROP TABLE iF EXISTS REFRESH_TOKENS CASCADE ;")
                await curr.execute("""CREATE TABLE ACCOUNTS (id serial PRIMARY KEY,
                                           first_name VARCHAR(255),
                                           last_name VARCHAR (255),
                                           password VARCHAR (255),
                                           social_id VARCHAR (100) UNIQUE ,
                                           email VARCHAR (255));""")
                await curr.execute("""CREATE TABLE PROFILES (id serial PRIMARY KEY,
                                          account_id int REFERENCES accounts(id),
                                          username VARCHAR (255) UNIQUE,
                                          profile_picture VARCHAR (255));""")

                await curr.execute("""CREATE TABLE REFRESH_TOKENS (id serial PRIMARY KEY,
                                                                   profile_id int REFERENCES profiles(id),
                                                                   refresh_token VARCHAR(300));""")



def app_factory(config_path:Optional[str]=None):
    app = Sanic(__name__)
    if config_path is not None:
        app.config.from_pyfile(config_path)
    from .blueprints import accounts, profile, oauth2, main
    app.blueprint(accounts, url_prefix="/accounts")
    app.blueprint(profile, url_prefix="/profile")
    app.blueprint(oauth2, url_prefix="/authorization/oauth2")
    app.blueprint(main, url_prefix="/authentication")

    @app.listener("before_server_start")
    async def setup(app, loop):
        app.redis_pool = await aioredis.create_redis_pool(
            'redis://user_service_cache',
            minsize=5,
            maxsize=10,
            loop=loop)

        app.pg_pool = await aiopg.create_pool(app.config["PG_DSN"])
        app.aiohttp_session = aiohttp.ClientSession()
        await prep_db(app.config["PG_DSN"])


    @app.listener("after_server_stop")
    async def teardown(app,loop):
        app.redis_pool.close()
        await app.redis.wait_closed()
        await app.aiohttp_sesson.closed()
    return app