from sanic_oauth.providers import GoogleClient, LinkedinClient, FacebookClient


def oauth2_client_factory(client:str):
    oauth2_client = None
    if client == "google":
        oauth2_client = GoogleClient
    elif client == "linkedin":
        oauth2_client = LinkedinClient
    elif client == "facebook":
        oauth2_client = FacebookClient
    return oauth2_client