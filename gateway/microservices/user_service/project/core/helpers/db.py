import aiopg
from typing import List, Tuple, Any,Dict

async def find_user_with_oauth2(dsn:str, social_id) -> List[Tuple[Any]]:
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(f"""
                PREPARE find_user_with_social_id(text) AS SELECT email FROM ACCOUNTS WHERE social_id=$1;
                EXECUTE find_user_with_social_id(%s);
                """, (social_id,))
                data = [row async for row in cur]
                return data

async def create_user_with_oauth2(dsn:str, data:Dict[str, Any]=None) -> None:
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                fields = ",".join([field for field in data])
                values = ",".join([f"'{value}'" for value in data.values()])
                placeholders = ",".join([f"${i+1}"for i in len(values)])
                await cur.execute(f"""
                PREPARE create_user_with_social_id(text)
                AS INSERT INTO ACCOUNTS(%s) VALUES (%s);
                EXECUTE find_user_with_social_id(%s);
                """, (fields, placeholders, values))
                data = [row async for row in cur]
                return data



