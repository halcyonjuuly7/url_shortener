import aiopg

from . import oauth2
from sanic import response
from core.helpers.oauth2 import oauth2_client_factory
from core.helpers.db import find_user_with_oauth2, create_user_with_oauth2


@oauth2.route("/authorize/<provider>", methods=("POST",))
async def provider(request, provider):
    oauth2_provider_creds=request.app.config['OAUTH2_CREDS'][provider]
    print(oauth2_provider_creds)
    oauth2_client = oauth2_client_factory(provider)(request.app.aiohttp_session,
                                                    oauth2_provider_creds["consumer_key"],
                                                    oauth2_provider_creds["consumer_secret"])
    try:
        auth_url = oauth2_client.get_authorize_url(scope='email',
                                                   response_type='code',
                                                   redirect_uri=request.json.get("redirect_uri"),
                                                   provider=provider)
        resp = {"status": "success", "auth_url": auth_url}
    except Exception as e:
        resp = {"status": "error", "auth_url": None ,"message": "an error occured"}
    return response.json(resp)

@oauth2.route("/callback/<provider>", methods=("POST", ))
async def provider_callback(request, provider):
    code = request.json.get("code")
    if code is None:
        return response.json({"status": "error", "message": "an unknown error occured"})
    try:
        oauth2_provider_creds = request.app.config['OAUTH2_CREDS'][provider]
        oauth2_client = oauth2_client_factory(provider)(request.app.aiohttp_session,
                                                        oauth2_provider_creds["consumer_key"],
                                                        oauth2_provider_creds["consumer_secret"])
        access_token = await oauth2_client.get_access_token(code, redirect_uri=request.json.get("redirect_uri"))
        user_info, _ = await oauth2_client.get_user_info(access_token=access_token)
        pg_dsn = request.app.config["PG_DSN"]
        info = {"social_id": f"{provider}-{user_info.id}",
                "first_name": user_info.first_name,
                "last_name": user_info.last_name,
                "email": user_info.email}
        user_account = await find_user_with_oauth2(pg_dsn, data={"social_id": info["social_id"]})
        if not user_account:
            await create_user_with_oauth2(pg_dsn, data=info)
        resp = {"status": "success",  "user_info": info}
    except Exception as e:
        resp = {"status": "error", "message": "an unknown error occured", "access_token": None}
    return response.json(resp)


