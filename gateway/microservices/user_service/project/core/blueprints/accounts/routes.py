from . import accounts
from sanic import response
import bcrypt
import aiopg

@accounts.route("/<username>")
async def account_index(request, username):
    pass

@accounts.route("/create", methods=("POST",))
async def create_account(request):

    async with aiopg.create_pool(request.app.config["PG_DSN"]) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as curr:
                await curr.execute(f"""
                PREPARE get_account_info(text) AS SELECT * FROM ACCOUNTS WHERE email = $1 LIMIT 1;
                EXECUTE get_account_info(%s)""", (request.json.get('email'),))
                user = [row async for row in curr]
                if not user:
                    data = request.json
                    await curr.execute(f"""
                PREPARE create_account(text, text, text, text) AS INSERT INTO ACCOUNTS (first_name, last_name, email, password) VALUES ($1, $2, $3, $4) RETURNING id;
                EXECUTE create_account(%s, %s, %s, %s);""",
                                                    (data['first_name'],
                                                     data['last_name'],
                                                     data['email'],
                                                     bcrypt.hashpw(data['password'].encode(),
                                                                   bcrypt.gensalt()).decode()))
                    account_id = [row async for row in curr]
                    await curr.execute(f"""
                    PREPARE create_profile(int, text) AS INSERT INTO PROFILES (account_id, username) VALUES ($1, $2) RETURNING id;
                    EXECUTE create_profile(%s, %s);""", (account_id[0], f"{data['first_name']}-{data['last_name']}-{account_id[0]}"))
                    profile_id = [row async for row in curr]
                    return response.json({"status": "success", "profile_id": profile_id[0][0]})
                return response.json({"status": "error", "message": "email already exists"})


@accounts.route("/info", methods=("GET",))
async def get_account_info(request):
    async with aiopg.create_pool(request.app.config["PG_DSN"]) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as curr:
                await curr.execute("""
                PREPARE get_account_info(text) AS SELECT * FROM ACCOUNTS WHERE email = $1 LIMIT 1;
                EXECUTE get_account_info(%s)""", (request.args.get('email'),))
                data = [row async for row in curr]
                return response.json({
                    "status": "success",
                    "data": [dict(zip([col[0] for col in curr.description], data[0]))]
                })

@accounts.route("/check_user_password", methods=("POST", ))
async def check_user_password(request):

    async with aiopg.create_pool(request.app.config["PG_DSN"]) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as curr:
                await curr.execute("""
                                PREPARE get_account_info(text) AS SELECT password FROM ACCOUNTS WHERE email = $1 AND social_id IS NULL LIMIT 1;
                                EXECUTE get_account_info(%s)""", (request.json.get('email'),))
                data = [row async for row in curr]
                if data:
                    if bcrypt.checkpw(request.json.get("password").encode(),
                                      data[0][0].encode()):
                        return response.json({"status": "success", "msg": "passwords match for users"})
                    return response.json({"status": "error", "msg": "password mismatch"})
                return response.json({"status": "error", "msg": "unknown user"})



@accounts.route("/user_info")
async def get_prof_info(request):
    async with aiopg.create_pool(request.app.config["PG_DSN"]) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as curr:
                await curr.execute("""
                SELECT ACCOUNTS.first_name, ACCOUNTS.last_name, ACCOUNTS.email, ACCOUNTS.social_id, PROFILES.id AS profile_id FROM ACCOUNTS JOIN PROFILES ON ACCOUNTS.id = PROFILES.account_id WHERE ACCOUNTS.email = %s LIMIT 1;
                """, (request.args.get("email"),))
                data = [row async for row in curr]
                if data:
                    return response.json({"status": "success",
                                          "data": dict(zip([col[0] for col in curr.description], data[0]))})
    return response.json({"status": "error"})


