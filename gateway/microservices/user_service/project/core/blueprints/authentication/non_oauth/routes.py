import jwt
import datetime
from . import main
from sanic import response
import base64

@main.route("/create_token", methods=("POST",))
async def create_token(request):
    try:
        token = base64.b64encode(jwt.encode(
            {
                "data": request.json
            },
            request.app.config.get("SECRET_KEY"),
            request.app.config.get("JWT_ALGORITHM"))).decode()
        resp = {
            "status": "success",
            "token": token
        }
    except Exception as e:
        resp = {
            "status": "error",
            "message": e,
            "token": None
        }
    return response.json(resp)

@main.route("/decode_token", methods=("POST",))
async def validate_token(request):
    token = request.json.get("auth_token")
    try:
        payload = jwt.decode(base64.b64decode(token).decode(),
                             request.app.config["SECRET_KEY"],
                             request.app.config.get("JWT_ALGORITHM"))
        resp = {"status": "success", "message": "valid token", "payload": payload}
    except (jwt.ExpiredSignatureError, jwt.InvalidTokenError):
        resp = {"status": "error", "message": "invalid token"}
    return response.json(resp)

@main.route("/blacklist_token", methods=("POST",))
async def blacklist_token(request):
    token = request.json.get('auth_token')
    await request.app.redis_pool.setex(token, 3000, token)
    return response.json({"status": "success", "msg": "token black listed"})



@main.route("/validate_token", methods=("POST",))
async def validate_token(request):
    token_blacklist = await request.app.redis_pool.get(request.json.get("auth_token"))
    if token_blacklist:
        return response.json({"status": "error", "msg": "token blacklisted"})
    return response.json({"status": "success", "msg": "token not blacklisted"})

