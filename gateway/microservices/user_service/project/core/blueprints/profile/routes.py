import aiopg

from . import profile
from sanic import response


@profile.route("/<username>", methods=("GET",))
async def index(request, username):
    async with aiopg.create_pool(request.app.config["PG_DSN"]) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(f"EXECUTE get_profile(%s);", ("", ))



@profile.route("/info", methods=("GET",))
async def profile_info(request):
    async with aiopg.create_pool(request.app.config["PG_DSN"]) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as curr:
                await curr.execute("""
                                PREPARE get_profile_info(int) AS SELECT * FROM PROFILES WHERE id = $1 LIMIT 1;
                                EXECUTE get_profile_info(%s);""", (int(request.args.get('profile_id')),))
                data = [row async for row in curr]
                if data:
                    return response.json({
                        "status": "success",
                        "data": [dict(zip([row[0] for row in curr.description], data[0]))]
                    })
                return response.json({"status": "error"})

