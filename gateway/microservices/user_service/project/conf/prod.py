import os
SECRET_KEY = "cLPFt3so8S0SbLVCusYPS1QToEhEq/uZ"
SERVER_NAME="https://url-shortener.zeon-labs.services"
MONGO_URI = "mongodb://mongo:27017/hashes"
POSTGRES_CREDS = {
    "PG_USER": os.environ.get("PG_USER", 'postgres'),
    "PG_PASSWORD": os.environ.get("PG_PASSWORD", '12345!!!'),
    "PG_DB": os.environ.get("PG_DB", "users"),
    "PG_HOST": os.environ.get("PG_HOST", "postgres")
}
PG_DSN = f'dbname={os.environ.get("PG_DB", "users")} user={os.environ.get("PG_USER", "postgres")} password={os.environ.get("PG_PASSWORD", "12345!!!")} host={os.environ.get("PG_HOST", "postgres")}'

PREPED_STATEMENTS = [
    "PREPARE create_account(text, text, text, text) AS INSERT INTO accounts (first_name, last_name, email, password) VALUES ($1, $2, $3, $4) RETURNING id;",
    "PREPARE get_account_info(text) AS SELECT * FROM ACCOUNTS WHERE email = $1 LIMIT 1;",
    "PREPARE create_profile(int, text) AS INSERT INTO PROFILES (account_id, username) VALUES ($1, $2);",
    "PREPARE get_prof_info(text) AS SELECT PROFILE.* FROM PROFILE JOIN ACCOUNTS ON PROFILE.account_id = ACCOUNTS.id WHERE ACCOUNTS.email = $1 LIMIT 1;"

]

OAUTH2_CREDS = {
    "google": {
        "consumer_secret": "",
        "consumer_key": ""
    },
    "facebook": {
        "consumer_secret": "e15ddbd14f676a511e8a75a1d9eebf6d",
        "consumer_key": "1748027101958015"
    },
    "github": {
        "consumer_secret": "",
        "consumer_key": ""
    },
    "linkedin": {
        "consumer_secret": "",
        "consumer_key": ""
    }
}

AUTH_COOKIE = "ZEON-LABS-AUTH"
JWT_ALGORITHM="HS256"