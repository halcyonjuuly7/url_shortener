import aiohttp
import aioredis
from sanic import Sanic
from typing import Optional



def app_factory(config_path:Optional[str]=None) -> Sanic:
    app = Sanic(__name__)
    if config_path is not None:
        app.config.from_pyfile(config_path)

    from .blueprints import main
    app.blueprint(main, url_prefix="")

    @app.listener("before_server_start")
    async def setup(app, loop):
        app.session = aiohttp.ClientSession()
        app.redis = await aioredis.create_redis_pool(
            'redis://redir_cache',
            minsize=5,
            maxsize=10,
            loop=loop)


    @app.listener("after_server_stop")
    async def teardown(app, loop):
        await app.session.close()
    return app