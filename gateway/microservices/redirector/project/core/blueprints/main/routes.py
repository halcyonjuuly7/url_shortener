from . import main
import os
import asyncio
from sanic import response

@main.route("/<hash>", methods=("GET", ))
async def get_hash_url(request, hash):
    cached_url = await request.app.redis.get(hash)
    if not cached_url:
        async with request.app.session.get("http://key_service:5555/get_url", params={"key": hash}) as resp:
            data = await resp.json()
        if data["status"] == "success":
            await request.app.redis.set(hash, data["url"])
            async with request.app.session.post("http://key_service:5555/incr_key", json={"hash": hash}) as resp:
                incr_data = await resp.json()
            return response.redirect(data['url'])
        return response.redirect(f"{request.app.config['WEB_URL']}/404.html")
    return response.redirect(cached_url.decode())
