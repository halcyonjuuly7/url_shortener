import string
SECRET_KEY="brhwbrabeweauoewnn1213"
HOST_NAME="https://url-shortener.zeon-labs.services"
VALID_ALIAS_CHARS = set(f"{string.ascii_letters}{string.digits}-")
JWT_ALGO="HS256"
AUTH_COOKIE="ZEON-LABS-AUTH"