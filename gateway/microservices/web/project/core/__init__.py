from flask import Flask
from typing import Optional


def app_factory(config_path:Optional[str]=None):
    app = Flask(__name__)
    if config_path is not None:
        app.config.from_pyfile(config_path)

    from .blueprints import index, account, profile
    app.register_blueprint(account, url_prefix="/account")
    app.register_blueprint(profile, url_prefix="/profile")
    app.register_blueprint(index)
    return app

