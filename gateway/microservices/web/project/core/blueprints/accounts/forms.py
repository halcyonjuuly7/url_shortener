from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    email = StringField("email")
    password = PasswordField("password")
    google_oauth2 = SubmitField("Sign in using Google")
    facebook_oauth2 = SubmitField("Sign in using Facebook")
    login = SubmitField("login")



class SignUpForm(FlaskForm):
    first_name = StringField("First Name")
    last_name = StringField("Last Name")
    email = StringField("Email")
    password = PasswordField("password")
    password2 = PasswordField("re-type password")
    facebook_oauth2 = SubmitField("Sign up using facebook")
    google_oauth2 = SubmitField("Sign up using google")
    signup = SubmitField("Sign Up")
