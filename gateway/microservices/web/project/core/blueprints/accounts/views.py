import requests
from . import account
from flask import render_template, redirect, make_response, url_for, request, current_app
from core.helpers.security import login_required, encode_jwt, decode_jwt
from core.helpers.oauth2 import oauth2_signup_and_login
from .forms import LoginForm, SignUpForm

@account.route("/login", methods=("GET", "POST"))
def login():
    auth_cookie = request.cookies.get(current_app.config["AUTH_COOKIE"])
    if auth_cookie:
        resp = requests.post("http://web_gateway:5555/authentication/validate_token",
                             json={"auth_token": auth_cookie}).json()
        if resp['status'] == "success":
            return redirect(url_for("profile.index"))

    login_form = LoginForm()
    signup_form = SignUpForm()

    if login_form.validate_on_submit() and (login_form.login.data or
                                            login_form.facebook_oauth2.data or
                                            login_form.google_oauth2.data):
        if login_form.facebook_oauth2.data or login_form.google_oauth2.data:
            provider = "facebook" if login_form.facebook_oauth2.data else "google"
            data = oauth2_signup_and_login(provider)
            print(data)
            if data["status"] == "success":
                return redirect(data["auth_url"])
        else:
            email = login_form.email.data.strip()
            login_data = requests.post("http://web_gateway:5555/accounts/login_user",
                          json={"email": email,
                                "password": login_form.password.data.strip()}).json()
            if login_data["status"] == "success":
                resp = make_response(redirect(url_for("profile.index")))
                resp.set_cookie(current_app.config["AUTH_COOKIE"], login_data["token"])
                return resp

    elif signup_form.validate_on_submit() and (signup_form.signup.data or
                                               signup_form.google_oauth2.data or
                                               signup_form.facebook_oauth2.data):
        if signup_form.google_oauth2.data or signup_form.facebook_oauth2.data:
            provider = "google" if signup_form.google_oauth2.data else "facebook"
            data = oauth2_signup_and_login(provider)
            if data["status"] == "success":
                return redirect(url_for(data["auth"]))
        else:
            data = requests.post("http://web_gateway:5555/accounts/create", json={
                "first_name": signup_form.first_name.data,
                "last_name": signup_form.last_name.data,
                "password": signup_form.password.data,
                "email": signup_form.email.data
            }).json()
            if data["status"] == "success":
                resp = make_response(redirect(url_for('profile.index')))
                resp.set_cookie(current_app.config["AUTH_COOKIE"], data["token"])
                return resp
            return redirect(url_for("account.login"))
    return render_template("login.html", login_form=login_form, signup_form=signup_form)

@account.route("/logout")
@login_required
def account_logout():
    requests.post("http://web_gateway:5555/authentication/blacklist_token",
                  json={'auth_token': request.cookies.get(current_app.config["AUTH_COOKIE"])})
    response = make_response(redirect(url_for('account.login')))
    response.set_cookie(current_app.config["AUTH_COOKIE"], expires=0)
    return response

@account.route("/forgot_password", methods=("GET", "POST"))
def forgot_password():
    pass



@account.route("/oauth2/callback/<provider>")
def oauth_callback(provider):
    redir_uri = f"{current_app.config['HOST_NAME']}/accounts/oauth2/callback/{provider}"
    user_info = requests.post(f"http://web_gateway:5555/authorization/oauth2/callback/{provider}",
                              json={"redirect_uri": redir_uri,
                                    "code": request.args.get("code")}).json()
    if user_info["status"] == "success":
        response = make_response(redirect(url_for('account.index')))
        response.set_cookie(current_app.config["AUTH_COOKIE"], user_info['token'])
        return redirect(url_for("profile.index"))
    return redirect(url_for("account.login"))




