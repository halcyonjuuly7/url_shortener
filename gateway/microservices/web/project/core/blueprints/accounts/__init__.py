from flask import Blueprint

account = Blueprint("account",
                    __name__,
                    template_folder="templates",
                    static_folder="static",
                    static_url_path="/account/static")

from . import views