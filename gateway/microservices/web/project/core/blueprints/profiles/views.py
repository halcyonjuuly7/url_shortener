import requests

from . import profile
from flask import render_template, redirect, current_app,request
from core.helpers.security import login_required

@profile.route("/")
@login_required
def index():
    data = requests.post("http://web_gateway:5555/authentication/decode_token",
                         json={"auth_token": request.cookies.get(current_app.config["AUTH_COOKIE"])}).json()
    print(data)
    prof_info = requests.get("http://web_gateway:5555/profile/info",
                             params={"profile_id": data["payload"]["data"]["profile_id"]}).json()
    return render_template("index.html", prof_info=prof_info)