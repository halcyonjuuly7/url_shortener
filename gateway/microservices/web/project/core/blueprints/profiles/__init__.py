from flask import Blueprint

profile = Blueprint("profile",
                __name__,
                static_folder="static",
                static_url_path="/profile/static",
                template_folder="templates")

from . import views