import string

from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError
from wtforms import StringField, SubmitField, SelectField

class UrlForm(FlaskForm):
    url = StringField("url", validators = [DataRequired()])
    alias = StringField("alias")
    shorten_url = SubmitField("shorten url")




