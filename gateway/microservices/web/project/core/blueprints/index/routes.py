import requests
import datetime
import base64


from flask import render_template, current_app, request, redirect, url_for
from . import index
from .forms import UrlForm
from core.helpers.security import validate_alias, encode_jwt, decode_jwt

@index.route("/", methods=("GET", "POST"))
def home():
    form = UrlForm()
    if form.validate_on_submit():


        token = encode_jwt(current_app.config["SECRET_KEY"],
                           current_app.config["JWT_ALGO"],
                           data={"iat": datetime.datetime.utcnow(),
                                 "exp": datetime.datetime.utcnow() + datetime.timedelta(seconds=30),
                                 "alias": form.alias.data.strip(),
                                 "url": form.url.data.strip()})

        return redirect(url_for('index.create_url',token=token), code=307)
    return render_template("home.html", form=form)

@index.route("/create_url", methods=("POST", ))
def create_url():
    data = decode_jwt(current_app.config["SECRET_KEY"],
                      current_app.config["JWT_ALGO"],
                      request.args.get("token"))
    if not data:
        return redirect(url_for("index.home"))

    alias = data.get("alias")
    msg = None
    if not validate_alias(alias, valid_chars=current_app.config["VALID_ALIAS_CHARS"]):
        alias = None
        msg = "the alias supplied contained invalid chars. so we just generated a new one"
    resp = requests.post("http://web_gateway:5555/key_service/anon_generate_key",
                         json={"url": data.get("url"),
                               "alias": alias}).json()
    resp["host_name"] = current_app.config.get('HOST_NAME')
    return render_template("create_url.html", content=resp, msg=msg)
