import functools
import requests
import jwt
import base64

from typing import Set
from flask import current_app, request, redirect, url_for


def validate_alias(alias: str, valid_chars:Set[str])->bool:
    for char in alias:
        if char not in valid_chars:
            return False
    return True

def encode_jwt(secret, algo, data=None):
    if not data:
        raise ValueError("Need data to encode")
    return base64.b64encode(jwt.encode(data, secret, algorithm = algo)).decode()

def decode_jwt(secret, algo, token):
    try:
        return jwt.decode(base64.b64decode(token).decode(), secret, algorithms=[algo])
    except Exception as e:
        print(e)
        return None



def login_required(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        auth_token = request.cookies.get(current_app.config["AUTH_COOKIE"])
        if auth_token:
            resp = requests.post("http://web_gateway:5555/authentication/validate_token",
                          json={"auth_token": auth_token})
            resp = resp.json()
            if resp["status"] == "success":
                return f(*args, **kwargs)
        return redirect(url_for("account.login"))
    return wrapper