from flask import current_app
import requests


def oauth2_signup_and_login(provider):
    redirect_uri = f"{current_app.config['HOST_NAME']}/account/oauth2/callback/{provider}"
    data = requests.post(f"http://web_gateway:5555/authorization/oauth2/authorize/{provider}",
                         json={"redirect_uri": redirect_uri}).json()
    return data