from core import app_factory
import os

config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "conf", "prod.py")
application = app_factory(config_path=config_path)

if __name__ == "__main__":
    application.run("0.0.0.0", port=5555)