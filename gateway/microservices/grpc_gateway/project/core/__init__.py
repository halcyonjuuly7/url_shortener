from sanic import Sanic
from typing import Optional

def app_factory(config_path:Optional[str] = None):

    app = Sanic(__name__)
    if config_path is not None:
        app.config.from_pyfile(config_path)
    return app

