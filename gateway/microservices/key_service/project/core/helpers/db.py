import aiopg

async def get_hash(dsn, key, cols=None):
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(f"""
                PREPARE find_one(text) AS SELECT {','.join(cols) if cols else '*'} FROM URLS WHERE hash=$1;
                EXECUTE find_one('{key}');
                """)
                data = [item async for item in cur]
                return data

async def fetch_url(dsn, url):
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(f"""
                PREPARE get_url(text) AS SELECT hash FROM URLS WHERE target = $1;
                EXECUTE get_url('{url}');
                """)
                data = [row async for row in cur]
                return data

async def insert_url(dsn, **values):
    if values:
        async with aiopg.create_pool(dsn) as pool:
            async with pool.acquire() as conn:
                async with conn.cursor() as cur:
                    cols = ','.join(values.keys())
                    inputs = values.values()
                    val_lengths = ",".join([f"${i}" for i in range(1, len(inputs) + 1)])
                    formatted_inputs = ','.join(map(lambda item: f"'{item}'", inputs))
                    await cur.execute(f"""
                    PREPARE insert_hash AS INSERT INTO URLS({cols}) VALUES ({val_lengths});
                    EXECUTE insert_hash({formatted_inputs});
                    """)
    else:
        raise ValueError("Nothing to insert")

async def increment_access(dsn, key):
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(f"""
                PREPARE increment_access(text) AS
                UPDATE URLS SET redirects = redirects + 1 WHERE hash = $1;
                EXECUTE increment_access('{key}');
                """)


async def get_profile_urls(dsn, profile_id):
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute("SELECT * FROM URLS WHERE owner = %s", (profile_id,))
                fields = [row[0] for row in cur.description]
                data = [row async for row in cur]
                if data:
                     return [dict(zip(fields, item)) for item in data]
                return []
