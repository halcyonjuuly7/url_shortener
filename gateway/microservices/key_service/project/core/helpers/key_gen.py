import base64,random, string
import hashlib


class KeyGenerator:
    @staticmethod
    def gen_shortkey(url:str, shortkey_length:int=0) -> str:
        key = KeyGenerator.generate_key(url)
        return key[-shortkey_length:]

    @staticmethod
    def generate_key(url:str)-> str:
        md5 = hashlib.md5(url.encode()).hexdigest()
        return base64.b64encode(md5.encode()).decode()
