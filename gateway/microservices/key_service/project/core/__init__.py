import aioredis
import aiopg

from sanic import Sanic
from typing import Optional


async def prep_db(pg_pool):
    async with pg_pool as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute("DROP TABLE IF EXISTS URLS;")
                await cur.execute("""CREATE TABLE URLS (id SERIAL PRIMARY KEY,
                                           created_at TIMESTAMP,
                                           hash VARCHAR (255),
                                           target VARCHAR (1000),
                                           redirects int,
                                           owner int)""")



def app_factory(config_path:Optional[str]=None):
    app = Sanic(__name__)
    if config_path is not None:
        app.config.from_pyfile(config_path)

    from .blueprints import key_handler, profiles

    app.blueprint(key_handler)
    app.blueprint(profiles, url_prefix="/profile")

    @app.listener("before_server_start")
    async def setup(app, loop):
        app.redis = await aioredis.create_redis_pool(
            'redis://key_service_cache',
            minsize=5,
            maxsize=10,
            loop=loop)
        app.pg_pool = await aiopg.create_pool(app.config["PG_DSN"])
        await prep_db(app.pg_pool)

    @app.listener("after_server_stop")
    async def teardown(app, loop):
        app.redis.close()
        await app.redis.wait_closed()
    return app