from sanic import response
from . import profiles
from core.helpers.db import get_profile_urls

@profiles.route("/<profile_id>", methods=("GET",))
async def get_profile_keys(request, profile_id):
    prof_urls = await get_profile_urls(request.app.config["PG_DSN"], profile_id)
    return response.json({"status": "success", "urls": prof_urls})
