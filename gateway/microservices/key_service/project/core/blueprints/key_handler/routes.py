from . import key_handler
from core.helpers.key_gen import KeyGenerator
from core.helpers.db import get_hash, insert_url, fetch_url, increment_access
from sanic import response

# @key_handler.route("/generate_key", methods=("POST", ))
# async def gen_key(request):
#     key = request.json.get("alias")
#     if key:
#         status = {"status": "alias exists", "message": "alias exists", "alias": key}
#         doc = await request.app.motor.hashes.find_one({"hash": key})
#         if doc is None:
#             await request.app.motor.hashes.insert_one({"hash": key, "url": request.json.get("url")})
#             status = {"key": key, "status": "success"}
#     else:
#         prev_url = await request.app.motor.hashes.find_one({"url": request.json.get('url')})
#         if not prev_url:
#             key = generate_key(7)
#             doc = await request.app.motor.hashes.find_one({"hash": key})
#             while doc is not None:
#                 key = generate_key(7)
#                 doc = await request.app.motor.hashes.find_one({"hash": key})
#             await request.app.motor.hashes.insert_one({"hash": key, "url": request.json.get("url")})
#             status = {"key": key, "status": "success"}
#         else:
#             status = {"key": prev_url['hash'], "status": "success"}
#     return response.json(status)

@key_handler.route("/generate_key", methods=("POST", ))
async def gen_key(request):
    key = request.json.get("alias")
    if key:
        status = {"status": "alias exists", "message": "alias exists", "alias": key}
        doc = await get_hash(request.app.config["PG_DSN"], key)
        if not doc:
            await insert_url(request.app.config["PG_DSN"],
                              hash=key,
                              target=request.json.get("url"),
                              owner=request.json.get("owner", -1))
            status = {"key": key, "status": "success"}
    else:
        prev_url = await fetch_url(request.app.config["PG_DSN"], request.json.get("url"))
        if not prev_url:
            target = request.json.get("url")
            shortkey_length = request.app.config["SHORTKEY_LENGTH"]
            key = KeyGenerator.gen_shortkey(target,shortkey_length)
            doc = await get_hash(request.app.config["PG_DSN"], key)
            while doc:
                key = KeyGenerator.gen_shortkey(target,shortkey_length)
                doc = await get_hash(request.app.config["PG_DSN"], key)
            await insert_url(request.app.config["PG_DSN"],
                              hash=key,
                              target=request.json.get("url"),
                              owner=request.json.get("owner", -1))
            status = {"key": key, "status": "success"}
        else:
            status = {"key": prev_url[0][0], "status": "success"}
    return response.json(status)


@key_handler.route("/get_url", methods=("GET", ))
async def get_url(request):
    key = request.args.get("key")
    doc = await get_hash(request.app.config["PG_DSN"], key, cols=("target",))
    if doc:
        print(doc)
        return response.json({'url': doc[0][0], 'status': 'success'})
    return response.json({'status': 'error', 'message': 'unknown key'})

@key_handler.route("/incr_key", methods=("POST", ))
async def incr_key(request):
    try:
        await increment_access(request.app.config["PG_DSN"], request.json.get("hash"))
        resp = {"status": "success"}
    except:
        resp = {"status": "error", "message": "an error occured"}
    return response.json(resp)


# @key_handler.route("/get_url", methods=("GET", ))
# async def get_url(request):
#     key = request.args.get("key")
#     doc = await request.app.motor.hashes.find_one({"hash": key})
#     if doc is not None:
#         return response.json({'url': doc['url'], 'status': 'success'})
#     return response.json({'status': 'error', 'message': 'unknown key'})

