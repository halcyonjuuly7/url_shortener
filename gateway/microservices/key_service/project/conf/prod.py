import os

MONGO_URI = "mongodb://mongo:27017/hashes"
PG_DSN = f'dbname={os.environ.get("PG_DB", "users")} user={os.environ.get("PG_USER", "postgres")} password={os.environ.get("PG_PASSWORD", "12345!!!")} host={os.environ.get("PG_HOST", "postgres")}'
SHORTKEY_LENGTH=7
